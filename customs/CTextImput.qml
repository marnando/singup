import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import "../images"

Rectangle {
    id: root
    width: 100
    height: 40

    property bool isPass: false

//    Row {

//        Rectangle {
//            id: mImageRect
//            Image {
//                id: mTextImage
//                height: root.height
//                fillMode: Image.PreserveAspectFit
//                anchors {
//                    verticalCenter: root.verticalCenter
//                }
//            }
//        }

        TextField {
            id: m_imput
            style: TextFieldStyle {
                textColor: "black"
                font.pixelSize: 20

                background: Rectangle {
                    radius: 2

                    Image {
                        id: m_image
                        width: parent.width
                        source: "../images/fillButtonsBlue.png"
                        fillMode: Image.TileHorizontally
                        anchors.bottom: parent.bottom
                    }

                    border.color: "lightgray"
                    border.width: 1
                }
            }

            anchors {
                rightMargin: 0
                bottomMargin: 0
                leftMargin: 0
                topMargin: 0
                fill: parent
            }
        }
//    }

    function onSetText(a_text) {
        m_imput.placeholderText = a_text
    }

    function onSetEchoMode() {
        m_imput.echoMode = TextInput.Password
    }

    function onGetText() {
        return m_imput.getText()
    }

//    function onSetImageSource(a_path)
//    {
//        mTextImage.source = a_path
//    }
}



