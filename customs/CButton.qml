import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Rectangle {
    id: root
    width: 70
    height: 40

    Button {
        anchors.fill: parent

        Text {
            id: m_btntext
            color: "white"
            text: qsTr("Conecte-se")
            font {
                bold: true
                pixelSize: 20
            }

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

            anchors.fill: parent
        }

        style: ButtonStyle {
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 25
                border.width: control.activeFocus ? 2 : 1
                border.color: "white"
                radius: 2

                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#C3A28B" : "#5772AA" }
                    GradientStop { position: 0 ; color: control.pressed ? "#5772AA" : "#C3A28B" }
                }
            }
        }
    }

    function onSetText(a_text) {
        m_btntext.text = a_text
    }
}

