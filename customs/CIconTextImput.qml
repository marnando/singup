import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import "../images"

Rectangle {
    id: root
    width: 200
    height: 40
    anchors.verticalCenterOffset: -14

    property bool isPass: false

    Image {
        id: mImage
        width: root.height
        height: root.height
        anchors.left: root.left
    }

    TextField {
        id: mTextField
        height: root.height
        anchors {
            left: mImage.right
            right: root.right
        }

        style: TextFieldStyle {
            textColor: "black"
            font.pixelSize: 12
            background: Rectangle {
                radius: 2
                border.color: "lightgray"
                border.width: 1

                Image {
                    id: m_image
                    width: parent.width
                    source: "../images/fillButtonsBlue.png"
                    fillMode: Image.TileHorizontally
                    anchors.bottom: parent.bottom
                }

            }
        }
    }

    Image {
        id: mImageCheck
        width: root.height - 3
        height: root.height - 3
        visible: true
        anchors {
            right: root.right
        }
    }

    function onSetText(a_text) {
        mTextField.placeholderText = a_text
    }

    function onSetEchoMode() {
        mTextField.echoMode = TextInput.Password
    }

    function onSetIconTextField(a_path) {
        mImage.source = a_path
    }

    function onSetIconCheckField(a_path) {
        mImageCheck.source = a_path
    }

    function onGetText() {
        return mTextField.getText()
    }

    function onImageCheckVisible(a_isVisible) {
        mImageCheck.visible = a_isVisible
    }

}
