import QtQuick 2.0
import QtQuick.Layouts 1.1
import "../customs"

Rectangle {
    id: root
    width: 200
    height: 200

    //Signais da aplicação
    signal signal_efetuarLogin(string a_user, string a_pass, int error)
    signal signal_errorLogin(int a_type) //0 - Ok; 1 - User error; 2 - Pass error
    signal signal_callSignUp()

    property string user: ""
    property string password: ""

    ColumnLayout {
        id: rootColumnLayout

        anchors {
            top: parent.top
            topMargin: 50
            left: parent.left
            leftMargin: 20
            right: parent.right
            rightMargin: 20
        }

        CIconTextImput {
            id: m_textUser
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                right: parent.right
            }

            //Start of Application
            Component.onCompleted: {
                onSetText(qsTr("Usuário"))
                onSetIconTextField("../images/icons/human.png")
            }
        }

        CIconTextImput {
            id: m_textPass
            anchors {
                top: m_textUser.bottom
                topMargin: 10
                left: parent.left
                right: parent.right
            }

            //Start of Application
            Component.onCompleted: {
                onSetText(qsTr("Senha"))
                onSetIconTextField("../images/icons/key.png")
                onSetEchoMode() //Modo de edição de password com máscara
            }
        }

        CButton {
            id: m_buttonSignIn
            anchors {
                top: m_textPass.bottom
                topMargin: 10
                left: parent.left
                right: parent.right
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var error = verifyDataLogin()
                    if (error === 0) {
                        signal_efetuarLogin(user, password, error)
                    }
                }
            }
        }

        Text {
            id: loginText
            text: qsTr("<u>Registre-se aqui!</u>")
            font.pixelSize: 20

            anchors {
                top: m_buttonSignIn.bottom
                topMargin: 20
                horizontalCenter: parent.horizontalCenter
            }

            MouseArea {
                onClicked: {

                }
            }
        }
    }

    function verifyDataLogin()
    {
        var codError = 0;
        if (m_textUser.onGetText() === "") {
            codError = 1
        } else {
            user = m_textUser.onGetText();
        }

        if (codError === 0)
        {
            if (m_textPass.onGetText() === "") {
                codError = 2
            } else {
                password = m_textPass.onGetText();
            }
        }

        return codError;
    }
}
