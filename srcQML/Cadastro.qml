import QtQuick 2.0
import QtQuick.Layouts 1.1
import "../customs"

Rectangle {
    id: root
    width: 300
    height: 400

    Component.onCompleted: {
        //TextImput
        mEmail.onSetText(qsTr("Email"))
        mUser.onSetText(qsTr("Usuário"))
        mPass.onSetText(qsTr("Senha"))
        mPass.onSetEchoMode() //Modo de edição de password com máscara

        //Button
        mConfirma.onSetText(qsTr("Registrar"))
    }

    ColumnLayout {
        id: column1
        anchors.fill: parent

        Rectangle {
            id: rectangle1
            anchors {
                right: parent.right
                rightMargin: 0
                left: parent.left
                leftMargin: 0
                bottom: parent.bottom
                bottomMargin: 20
                top: parent.top
                topMargin: 20
            }

            CTextImput {
                id: mEmail
                anchors {
                    top: parent.top
                    topMargin: 50
                    right: parent.right
                    rightMargin: 20
                    left: parent.left
                    leftMargin: 20
                }

            }

            CTextImput {
                id: mUser
                anchors {
                    top: mEmail.bottom
                    topMargin: 10
                    left: parent.left
                    leftMargin: 20
                    right: parent.right
                    rightMargin: 20
                }
            }

            CTextImput {
                id: mPass
                anchors {
                    top: mUser.bottom
                    topMargin: 10
                    left: parent.left
                    leftMargin: 20
                    right: parent.right
                    rightMargin: 20
                }
            }

            CButton {
                id: mConfirma
                anchors {
                    top: mPass.bottom
                    topMargin: 20
                    right: parent.right
                    rightMargin: 20
                    left: parent.left
                    leftMargin: 20
                }
            }
        }
    }
}
