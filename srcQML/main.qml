import QtQuick 2.2
import QtQuick.Window 2.1
import "../customs"

Window {
    id: rootWindow
    visible: true
    width: 540
    height: 960

    property int codError: 0 //0 - Ok; 1 - User error; 2 - Pass error

    signal signal_test()

    MouseArea {
        onClicked: {rootWindow.signal_test()}
    }

    Rectangle  {
        color: "lightgray"
//        width: rootWindow.width
//        height: rootWindow.height
        anchors.fill: parent

        VisualItemModel  {
            id: itemModel

            Login {
                id: mLogin
                width: parent.parent.width
                height: parent.parent.height
            }

            Cadastro {
                id: mCadastro
                width: parent.parent.width
                height: parent.parent.height
            }
        }

        ListView {
            id: view
            model: itemModel

            anchors  {
                fill: parent;
                bottomMargin: 30
            }

            preferredHighlightBegin: 0
            preferredHighlightEnd: 0

            highlightMoveDuration: 250
            highlightRangeMode: ListView.StrictlyEnforceRange
            boundsBehavior: Flickable.StopAtBounds

            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem
        }

        //menu inferior
//        Rectangle {
//            width: parent.width
//            height: 30
//            color: "gray"

//            anchors  {
//                top: view.bottom;
//                bottom: parent.bottom
//            }

//            Row {
//                anchors.centerIn: parent
//                spacing: 20

//                //Repete de acordo com o número de elementos no model
//                Repeater  {
//                    model: itemModel.count

//                    Rectangle  {
//                        width: 5
//                        height: 5
//                        radius: 3
//                        color: view.currentIndex == index ? "blue" : "white"

//                        MouseArea  {
//                            width: 20
//                            height: 20
//                            anchors.centerIn: parent
//                            onClicked: view.currentIndex = index
//                        }
//                    }
//                }
//            }
//        }
    }
}
