TEMPLATE = app

QT += qml quick widgets sql network

SOURCES += src/main.cpp \
    src/dao/connection.cpp \
    src/model/login.cpp \
    src/model/controlapp.cpp

HEADERS  += src/dao/connection.h \
    src/model/login.h \
    src/model/controlapp.h

RESOURCES += qml.qrc

OTHER_FILES += srcQML/main.qml \
        srcQML/Login.qml \
        srcQML/Cadastro.qml \
        customs/CButton.qml \
        customs/CTextImput.qml \
        customs/CIconTextImput.qml

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
