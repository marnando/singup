#include <QGuiApplication>
#include "model/controlapp.h"
#include "iostream"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    ControlApp control;
    control.initApp();
    return app.exec();
}
