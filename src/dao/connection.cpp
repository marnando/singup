#include "connection.h"

#include <QObject>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QUrl>
#include <QUrlQuery>

Connection * Connection::m_db = NULL;
Connection::Connection(QObject *parent) :
    QObject(parent)
{
}

Connection::~Connection()
{
    Connection::release();
}

bool Connection::createConnection()
{
    //TODO TEST_MJ
    sendLoginRequest();
    return true;

//    // Conecto o database
//    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));

//    // Seto um nome para omeu database
//    m_db->setDatabaseName("teste");

//    // Verifico se consigo carregá-lo
//    if (!m_db->open()) {

//        QMessageBox::critical(0
//                            , tr("Não pode abrir o Database")
//                            , tr("Não há uma conexão de estabilidade para um database.\n"
//                                 "Esse exemplo precisa de suporte ao SQLite. Por favor leia "
//                                 "a documentation Qt SQL driver para informações de como "
//                                 "executá-lo.\n\n"
//                                 "Clique em cancelar para finalizar.")
//                            , QMessageBox::Cancel);

//        return false;
//    } else {
//        QMessageBox::information(0
//                            , tr("Database Conectado")
//                            , tr("Database conectado com sucesso!!.")
//                            , QMessageBox::Ok);
//    }

    return true;
}

Connection *Connection::instance()
{
    if (!m_db)
        createConnection();

    return m_db;
}

void Connection::release()
{
    if (m_db)
    {
//        m_db->close();
        delete m_db;
        m_db = NULL;
    }
}

void Connection::sendLoginRequest()
{
    //Cria um loop de eventos temporário no loop de eventos principal
    QEventLoop eventLoop;

    QNetworkAccessManager manager;
    connect(&manager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest request( QUrl( QString("http://signup-server.herokuapp.com/login") ) );
    QNetworkReply * reply;

    QUrlQuery params;

    params.addQueryItem("username", "sergiosvieira" );
    params.addQueryItem("password", "senha" );

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded;charset=UTF-8");
    request.setHeader(QNetworkRequest::ContentLengthHeader, params.toString(QUrl::FullyEncoded).length());

    reply = manager.post(request, params.toString(QUrl::FullyEncoded).toUtf8());

    //Bloqueia a pilha de eventos até que o finished() seja chamado
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Success: " << reply->readAll();
        delete reply;
    } else {
        qDebug() << "Failure: " << reply->errorString();
        delete reply;
    }
}

bool Connection::signUp(QString a_mail, QString a_user, QString a_pass)
{
    QEventLoop eventLoop;

    QNetworkAccessManager manager;
    connect(&manager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest request( QUrl( QString("http://signup-server.herokuapp.com/register") ) );
    QNetworkReply * reply;

    QUrlQuery params;

//    params.addQueryItem("email", "username", "sergiosvieira" );
//    params.addQueryItem("email", "password", "senha" );

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded;charset=UTF-8");
    request.setHeader(QNetworkRequest::ContentLengthHeader, params.toString(QUrl::FullyEncoded).length());

    reply = manager.post(request, params.toString(QUrl::FullyEncoded).toUtf8());

    //Bloqueia a pilha de eventos até que o finished() seja chamado
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Success: " << reply->readAll();
        delete reply;
    } else {
        qDebug() << "Failure: " << reply->errorString();
        delete reply;
    }


    return true;
}
