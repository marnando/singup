#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class Connection : public QObject
{
    Q_OBJECT

//private:
//    QNetworkAccessManager m_manager;
//    QNetworkRequest m_request;
//    QNetworkReply m_reply;

public:
    explicit Connection(QObject *parent = 0);
    virtual ~Connection();

//Static functions
    static bool createConnection();
    static Connection *instance();
    static void release();
    static void sendLoginRequest();
    static bool signUp(QString a_mail, QString a_user, QString a_pass);
private:
    static Connection * m_db;

signals:
    void signal_criarLista();

public slots:
//    void sendRequest();

};

#endif // CONNECTION_H
