#include "controlapp.h"
#include "login.h"
#include <QDebug>

ControlApp::ControlApp(QObject *parent) :
    QObject(parent)
{

}

ControlApp::~ControlApp()
{
    if (m_login) {
        delete m_login;
        m_login = NULL;
    }
}

void ControlApp::initApp()
{
    //Se a application for um QML Window
    m_engine.load(QUrl(QStringLiteral("qrc:///srcQML/main.qml")));
    connect(&m_engine, SIGNAL(quit()), this, SLOT(deleteLater()));

    QObject * obj = m_engine.rootObjects().at(0);

    if (obj)
    {
        qDebug() << "Conectou 1: " <<
        connect(obj, SIGNAL(signal_efetuarLogin(QString, QString, int)), this, SLOT(onLogin(QString a_user, QString a_pass, int a_error)));

        qDebug() << "Conectou 2: " <<
        connect(obj, SIGNAL(signal_teste()), this, SLOT(oonTeste()));
    }

    login();
}

void ControlApp::login()
{
    m_login = new Login();
}

void ControlApp::onLogin(QString a_user, QString a_pass, int a_error)
{
    switch (a_error) {
    case 1: //user error

        break;

    case 2: //pass error

        break;

    default:
        break;
    }
}

void ControlApp::onTeste()
{
    switch (2) {
    case 1: //user error

        break;

    case 2: //pass error

        break;

    default:
        break;
    }
}
