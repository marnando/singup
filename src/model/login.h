#ifndef LOGIN_H
#define LOGIN_H

#include <QObject>

class Login : public QObject
{
    Q_OBJECT
public:
    explicit Login(QObject *parent = 0);
    ~Login();

    bool checkLogin(QString a_user, QString a_pass);
signals:

public slots:
    void on_efetuarLogin(QString a_user, QString a_password, int a_error);

};

#endif // LOGIN_H
