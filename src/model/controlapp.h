#ifndef CONTROLAPP_H
#define CONTROLAPP_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQuickView>

class Login;
class ControlApp : public QObject
{
    Q_OBJECT
public:
    QQmlApplicationEngine m_engine;
    QQuickView view;

    Login * m_login;

    explicit ControlApp(QObject *parent = 0);
    ~ControlApp();

    void initApp();
    void login();
signals:

public slots:
    void onLogin(QString a_user, QString a_pass, int a_error);
    void onTeste();

};

#endif // CONTROLAPP_H
